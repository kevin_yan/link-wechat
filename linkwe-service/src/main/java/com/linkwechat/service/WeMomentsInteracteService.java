package com.linkwechat.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.linkwechat.domain.WeMomentsInteracte;

public interface WeMomentsInteracteService extends IService<WeMomentsInteracte> {

}
